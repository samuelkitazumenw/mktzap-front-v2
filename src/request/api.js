import axios from 'axios';

const api = axios.create({
    baseURL: 'http://localhost/projects/mktzap-web/public/api'
})

export default api