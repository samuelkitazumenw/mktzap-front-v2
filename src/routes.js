import React from 'react'
import {BrowserRouter, Switch, Route} from 'react-router-dom';
import Login from './pages/Login'

export default function Routes() {
    return (
        <BrowserRouter basename="/mktzap-web/public/v2">
            <Switch>
                <Route path="/login" component={Login} />
            </Switch>
        </BrowserRouter>
    )
}