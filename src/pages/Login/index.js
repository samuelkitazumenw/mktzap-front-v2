import React, {useState} from 'react';
import './style.css';
import api from '../../request/api';

export default function Login() {
  
  const [email, setEmail] = useState('');
  const [password, setPass] = useState('');
  document.cookie = 'front=;expires=Thu, 01 Jan 1970 00:00:01 GMT;';

  async function handleSubmit(event) {
    event.preventDefault();
    try {
      const response = await api.post('/wolologin', { email, password });
      const { token } = response.data;
      const { success } = response.data;
      
      //setting cookie on front
      if (success) {
        (function(){
          var expdate = new Date();
          expdate.setTime(expdate.getTime()+(24*60*60*1000));
          document.cookie = `front=${encodeURI(JSON.stringify({ currentUser: { username: email, token }}))};expires=${expdate.toGMTString()}`;
        })();
      } else {
        console.error("E-mail e/ou senha inválidos");
      }
    } catch (error) {
      console.error(error);      
    }
  }
  
  return (
    <div className="shadow">
        <form onSubmit={handleSubmit}>
            <h1>Login</h1>
            <label htmlFor="email">Usuário *</label>
            <input
                id="email"
                type="text"
                onChange={event => setEmail(event.target.value)}
            />
            <label htmlFor="password">Senha *</label>
            <input
                id="password"
                type="password"
                onChange={event => setPass(event.target.value)}
            />
            <button className="btn" type="submit">Entrar</button>
        </form>
        <div id="galeria">
          &nbsp;
        </div>
    </div>
  );
}
